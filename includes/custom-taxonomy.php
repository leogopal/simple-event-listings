<?php

if (!function_exists('sel_event_locations')) {

  // Register Custom Taxonomy
  function sel_event_locations()
  {

    $labels = array(
      'name' => _x('Event Locations', 'Taxonomy General Name', 'sel'),
      'singular_name' => _x('Event Location', 'Taxonomy Singular Name', 'sel'),
      'menu_name' => __('Event Locations', 'sel'),
      'all_items' => __('All Event Locations', 'sel'),
      'parent_item' => __('Parent Event Location', 'sel'),
      'parent_item_colon' => __('Parent Event Location:', 'sel'),
      'new_item_name' => __('New Event Location Name', 'sel'),
      'add_new_item' => __('Add New Event Location', 'sel'),
      'edit_item' => __('Edit Event Location', 'sel'),
      'update_item' => __('Update Event Location', 'sel'),
      'view_item' => __('View Event Location', 'sel'),
      'separate_items_with_commas' => __('Separate Event Locations with commas', 'sel'),
      'add_or_remove_items' => __('Add or remove Event Locations', 'sel'),
      'choose_from_most_used' => __('Choose from the most used', 'sel'),
      'popular_items' => __('Popular Event Locations', 'sel'),
      'search_items' => __('Search Event Locations', 'sel'),
      'not_found' => __('Not Found', 'sel'),
      'no_terms' => __('No Event Locations', 'sel'),
      'items_list' => __('Event Locations list', 'sel'),
      'items_list_navigation' => __('Event Locations list navigation', 'sel'),
    );
    $rewrite = array(
      'slug' => 'event-location',
      'with_front' => true,
      'hierarchical' => false,
    );
    $args = array(
      'labels' => $labels,
      'hierarchical' => true,
      'public' => true,
      'show_ui' => true,
      'show_admin_column' => true,
      'show_in_nav_menus' => true,
      'show_tagcloud' => true,
      'rewrite' => $rewrite,
    );
    register_taxonomy('seleventlocations', array('seleventlistings'), $args);

  }

  add_action('init', 'sel_event_locations', 0);

}

if (!function_exists('sel_event_categories')) {

  // Register Custom Taxonomy
  function sel_event_categories()
  {

    $labels = array(
      'name' => _x('Event Categories', 'Taxonomy General Name', 'sel'),
      'singular_name' => _x('Event Category', 'Taxonomy Singular Name', 'sel'),
      'menu_name' => __('Event Categories', 'sel'),
      'all_items' => __('All Event Categories', 'sel'),
      'parent_item' => __('Parent Event Category', 'sel'),
      'parent_item_colon' => __('Parent Event Category:', 'sel'),
      'new_item_name' => __('New Event Category Name', 'sel'),
      'add_new_item' => __('Add New Event Category', 'sel'),
      'edit_item' => __('Edit Event Category', 'sel'),
      'update_item' => __('Update Event Category', 'sel'),
      'view_item' => __('View Event Category', 'sel'),
      'separate_items_with_commas' => __('Separate Event Categories with commas', 'sel'),
      'add_or_remove_items' => __('Add or remove Event Categories', 'sel'),
      'choose_from_most_used' => __('Choose from the most used', 'sel'),
      'popular_items' => __('Popular Event Categories', 'sel'),
      'search_items' => __('Search Event Categories', 'sel'),
      'not_found' => __('Not Found', 'sel'),
      'no_terms' => __('No Event Categories', 'sel'),
      'items_list' => __('Event Categories list', 'sel'),
      'items_list_navigation' => __('Event Categories list navigation', 'sel'),
    );
    $rewrite = array(
      'slug' => 'event-category',
      'with_front' => true,
      'hierarchical' => false,
    );
    $args = array(
      'labels' => $labels,
      'hierarchical' => true,
      'public' => true,
      'show_ui' => true,
      'show_admin_column' => true,
      'show_in_nav_menus' => true,
      'show_tagcloud' => true,
      'rewrite' => $rewrite,
    );
    register_taxonomy('seleventcategories', array('seleventlistings'), $args);

  }

  add_action('init', 'sel_event_categories', 0);

}