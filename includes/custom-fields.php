<?php
/**
 * Get the bootstrap! If using the plugin from wordpress.org, REMOVE THIS!
 */

// @TODO: 
// Start Date ­ only if is not a regular event
// Area
// More Info link
// Ticket link
// Categories with image upload field for that taxonomy
// 
add_action('cmb2_admin_init', 'event_regularity_field');
function event_regularity_field()
{

  $prefix = 'event_regularity_';

  $event_regularity = new_cmb2_box(array(
    'id' => $prefix . 'metabox',
    'title' => __('Event Regularity', 'sel'),
    'object_types' => array('seleventlistings'),
  ));

  $event_regularity->add_field(array(
    'name' => 'Is this a regular event?',
    'id' => $prefix . 'is_regular',
    'type' => 'radio_inline',
    'options' => array(
      'yes' => __('Yes', 'sel'),
      'no' => __('No', 'sel'),
    ),
    'default' => 'no',
  ));

}

add_action('cmb2_admin_init', 'event_option_fields');
function event_option_fields()
{

  $prefix = 'event_options_';

  $event_options = new_cmb2_box(array(
    'id' => $prefix . 'metabox',
    'title' => __('Event Details', 'sel'),
    'object_types' => array('seleventlistings'),
  ));

  $event_options->add_field(array(
    'name' => __('Start Date', 'sel'),
    'id' => $prefix . 'start_date',
    'type' => 'text_date_timestamp',
  ));

  $event_options->add_field(array(
    'name' => __('Area', 'sel'),
    'id' => $prefix . 'area',
    'type' => 'pw_map',
  ));


  $event_options->add_field(array(
    'name' => __('Area', 'sel'),
    'id' => $prefix . 'area',
    'type' => 'text',
  ));

  $event_options->add_field(array(
    'name' => __('More info link', 'sel'),
    'id' => $prefix . 'more_info_link',
    'type' => 'text_url',
  ));

  $event_options->add_field(array(
    'name' => __('Ticket link', 'sel'),
    'id' => $prefix . 'ticket_link',
    'type' => 'text_url',
  ));

}
