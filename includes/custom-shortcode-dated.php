<?php
if (!function_exists('sel_events_shortcode_dated')) {
  function sel_events_shortcode_dated($atts)
  {
    ob_start();

    extract(shortcode_atts(
        array(
          'location' => '',
          'type' => 'post',
          'order' => 'ASC',
          'orderby' => 'event_options_start_date',
          'posts' => -1,
        ), $atts, 'events-listing-dated')
    );


    // define query parameters based on attributes
    $options = array(
      'post_type' => 'seleventlistings',
      'meta_key' => 'event_options_start_date',
      'order' => $order,
      'orderby' => $orderby,
      'posts_per_page' => $posts,
      'location' => $location,
      'meta_query' => array(
        array(
          'key' => 'event_regularity_is_regular',
          'value' => 'no',
          'compare' => '='
        ),
        array(
          'key' => 'event_options_start_date',
          'value' => current_time('timestamp') - (24 * 3600),
          'compare' => '>'
        )
      ),
      'no_found_rows' => true,
    );

    $query = new WP_Query($options);
    if ($query->have_posts()) {
      global $post; ?>
      <div class="events-listing ">
        <?php $last_month = ''; ?>
        <?php while ($query->have_posts()) :
          $query->the_post();
          $event_title = the_title('<div class="event-title">', '</div>', false);
          $event_description = get_formatted_content();
          $event_date = get_post_meta(get_the_id(), 'event_options_start_date', true);
          $event_date_formatted = date("Y-m-d", $event_date);
          $current_time = current_time('timestamp');
          $time_until_event = human_time_diff($current_time, $event_date);
          $url_event_info = get_post_meta(get_the_id(), 'event_options_more_info_link', true);
          $url_event_ticket = get_post_meta(get_the_id(), 'event_options_ticket_link', true);
          $event_locations = get_the_terms(get_the_id(), 'seleventlocations');

          $current_month = date("F", $event_date);
          if ($current_month !== $last_month) {
            $last_month = $current_month;
            ?>
            <h3><?php echo $current_month; ?></h3><?php
          }
          ?>
          <div class="event">
            <div class="event-section">
              <a class="event-section-title" href="#event-<?php the_ID(); ?>">
                <div class="event-date">
                  <?php echo $event_date_formatted; ?>
                </div>
                <?php echo $event_title; ?>
                <div class="event-locations">
                  <?php
                  if ($event_locations && !is_wp_error($event_locations)) :
                    $location_links = array();
                    foreach ($event_locations as $event_location) {
                      $location_links[] = $event_location->name;
                    }
                    $event_locations_array = join(", ", $location_links);
                    echo esc_html($event_locations_array);
                  endif; ?>
                </div>
              </a>
              <div id="event-<?php the_ID(); ?>" class="event-section-content">
                <div class="event-description"><?php echo $event_description; ?></div>
                <div class="event-more-info">
                  <?php echo $url_event_info; ?>
                </div>
                <div class="event-register">
                  <?php echo $url_event_ticket; ?>
                </div>
              </div><!--end .event-section-content -->
            </div><!--end .event-section -->
          </div><!--end .event -->


        <?php endwhile;
        wp_reset_postdata(); ?>
      </div>

      <?php $output = ob_get_clean();
      return $output;
    } else {
      return "There are no events.";
    }
  }

  add_shortcode('events-listing-dated', 'sel_events_shortcode_dated');
}