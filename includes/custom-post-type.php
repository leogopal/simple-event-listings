<?php
if (!function_exists('sel_event_listing')) {

  // Register Custom Post Type
  function sel_event_listing()
  {

    $labels = array(
      'name' => _x('Event Listings', 'Post Type General Name', 'sel'),
      'singular_name' => _x('Event Listing', 'Post Type Singular Name', 'sel'),
      'menu_name' => __('Event Listings', 'sel'),
      'name_admin_bar' => __('Event Listings', 'sel'),
      'archives' => __('Event Listing Archives', 'sel'),
      'parent_item_colon' => __('Parent Item:', 'sel'),
      'all_items' => __('All Event Listings', 'sel'),
      'add_new_item' => __('Add New Event Listing', 'sel'),
      'add_new' => __('Add New', 'sel'),
      'new_item' => __('New Event Listings', 'sel'),
      'edit_item' => __('Edit Event Listing', 'sel'),
      'update_item' => __('Update Event Listing', 'sel'),
      'view_item' => __('View Event Listing', 'sel'),
      'search_items' => __('Search Event Listings', 'sel'),
      'not_found' => __('Not found', 'sel'),
      'not_found_in_trash' => __('Not found in Trash', 'sel'),
      'featured_image' => __('Featured Image', 'sel'),
      'set_featured_image' => __('Set featured image', 'sel'),
      'remove_featured_image' => __('Remove featured image', 'sel'),
      'use_featured_image' => __('Use as featured image', 'sel'),
      'insert_into_item' => __('Insert into Event Listing', 'sel'),
      'uploaded_to_this_item' => __('Uploaded to this Event Listing', 'sel'),
      'items_list' => __('Items Event Listing', 'sel'),
      'items_list_navigation' => __('Items Event Listing navigation', 'sel'),
      'filter_items_list' => __('Filter Event Listings list', 'sel'),
    );
    $rewrite = array(
      'slug' => 'events',
      'with_front' => true,
      'pages' => true,
      'feeds' => true,
    );
    $args = array(
      'label' => __('Event Listing', 'sel'),
      'description' => __('Simple Event Listings for WordPress', 'sel'),
      'labels' => $labels,
      'supports' => array('title', 'editor', 'author', 'thumbnail', 'revisions',),
      'taxonomies' => array('seleventlocations'),
      'hierarchical' => false,
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'menu_position' => 20,
      'menu_icon' => 'dashicons-calendar',
      'show_in_admin_bar' => true,
      'show_in_nav_menus' => true,
      'can_export' => true,
      'has_archive' => true,
      'exclude_from_search' => false,
      'publicly_queryable' => true,
      'rewrite' => $rewrite,
      'capability_type' => 'page',
    );
    register_post_type('seleventlistings', $args);

  }

  add_action('init', 'sel_event_listing', 0);

}