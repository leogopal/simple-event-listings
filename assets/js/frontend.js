jQuery(document).ready(function ($) {

  function close_event_section() {
    $('.event .event-section-title').removeClass('active');
    $('.event .event-section-content').slideUp(300).removeClass('open');
  }

  $('.event-section-title').click(function (e) {
    e.preventDefault();
    // Grab current anchor value
    var currentAttrValue = $(this).attr('href');

    if ($(this).is('.active')) {
      close_event_section();
    } else {
      close_event_section();

      // Add active class to section title
      $(this).addClass('active');
      // Open up the hidden content panel
      $('.event ' + currentAttrValue).slideDown(300).addClass('open');
    }

  });

});