<?php
/*
 * Plugin Name: Simple Event Listings
 * Version: 1.0
 * Plugin URI: http://leogopal.com/
 * Description: A very simple, short-code based, way to display events.
 * Author: Leo Gopal
 * Author URI: http://leogopal.com/
 * Requires at least: 4.5
 * Tested up to: 4.5
 *
 * Text Domain: simple-event-listings
 * Domain Path: /lang/
 *
 * @package WordPress
 * @author Leo Gopal
 * @since 1.0.0
 */

if (!defined('ABSPATH')) exit;

// Load plugin class files
add_action('plugins_loaded', 'load_requirements', 10);
function load_requirements()
{
  require_once('lib/vendor/webdevstudios/cmb2/init.php');
  require_once('includes/custom-post-type.php');
  require_once('includes/custom-taxonomy.php');
  require_once('includes/custom-fields.php');
  require_once('includes/custom-shortcode-dated.php');
  require_once('includes/custom-shortcode-regular.php');
}

/**
 * Frontend only
 */
add_action('wp_enqueue_scripts', 'enqueue_styles', 10);
function enqueue_styles()
{
  wp_register_style('-frontend', esc_url(plugins_url('/assets/', __FILE__)) . 'css/frontend.css', array(), '1.0');
  wp_enqueue_style('-frontend');
}

add_action('wp_enqueue_scripts', 'enqueue_scripts', 10);
function enqueue_scripts()
{
  wp_register_script('-frontend', esc_url(plugins_url('/assets/', __FILE__)) . 'js/frontend.js', array('jquery'), '1.0');
  wp_enqueue_script('-frontend');
}

/**
 * Admin only
 */
add_action('admin_enqueue_scripts', 'admin_enqueue_styles', 10, 1);
function admin_enqueue_styles()
{
  wp_register_style('-admin', esc_url(plugins_url('/assets/', __FILE__)) . 'css/admin.css', array(), '1.0');
  wp_enqueue_style('-admin');
}

add_action('admin_enqueue_scripts', 'admin_enqueue_scripts', 10, 1);
function admin_enqueue_scripts()
{
  wp_register_script('-admin', esc_url(plugins_url('/assets/', __FILE__)) . 'js/admin.js', array('jquery'), '1.0');
  wp_enqueue_script('-admin');
}